﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndoManager : MonoBehaviour {

    //一步操作
    public struct UndoData
    {
        public PokerContent DeleteContent;
        public List<PokerItem> DeleteItems;
        public List<PokerItem> OpenPokers;//如果有刚翻开的牌  记录  
    }

    List<UndoData> AllUndo=new List<UndoData>();
    //将操作加入到undo列表
    public void Add(PokerContent DeleteContent, List<PokerItem> DeleteItems,List<PokerItem> opens)
    {
        AllUndo.Add(new UndoData() { DeleteContent = DeleteContent, DeleteItems = DeleteItems, OpenPokers=opens });
    }
    //undo
    public void Undo()
    {
        bool needY = true;
        //while (needY)
        {
            needY = UndoLastOne();
        }
    }
    //返回是否需要再返回一步 
    //在达成收齐要求是这个时候做了两步操作  需要撤销两步
    bool UndoLastOne()
    {
        bool needY = false;
        if (AllUndo.Count > 0)
        {
            UndoData data = AllUndo[AllUndo.Count - 1];
            //当前组删除数据
            if (data.DeleteItems.Count > 0)
            {
                if (data.DeleteItems[0].ParentGroup.pointType== PokerContent.PointType.LeftBottom)
                {
                    needY = true;
                }
                for(int i=0;i< data.DeleteItems.Count; i++)
                {
                    data.DeleteItems[i].ParentGroup.Delete(new List<PokerItem>() { data.DeleteItems[i] }, false);
                }
                
            }
            //将数据添加到原组
            data.DeleteContent.Add(data.DeleteItems);
            //Debug.LogError(data.DeleteContent.name + "    " + data.DeleteItems.Count);
            for(int i=0;i<data.OpenPokers.Count;i++)
            {
                data.OpenPokers[i].Close();
            }
            //删除这条操作
            AllUndo.RemoveAt(AllUndo.Count - 1);
        }
        return needY;
    }
}
