﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PokerDrag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    //存储图片中心点与鼠标点击点的偏移量
    private Vector3 m_offset;
    private Vector3 m_dragStartPosition;
    //存储当前拖拽图片的RectTransform组件
    private RectTransform m_rt;
    void Start()
    {
        //初始化
        m_rt = gameObject.GetComponent<RectTransform>();
    }

     
    //开始拖拽触发
    public void OnBeginDrag(PointerEventData eventData)
    {
        m_dragStartPosition = transform.position;
        // 存储点击时的鼠标坐标
        Vector3 tWorldPos;
        //UI屏幕坐标转换为世界坐标
        RectTransformUtility.ScreenPointToWorldPointInRectangle(m_rt, eventData.position, eventData.pressEventCamera, out tWorldPos);
        //计算偏移量   
        m_offset = transform.position - tWorldPos;


        SetDraggedPosition(eventData);
    }

    //拖拽过程中触发
    public void OnDrag(PointerEventData eventData)
    {
        SetDraggedPosition(eventData);
    }

    //结束拖拽触发
    public void OnEndDrag(PointerEventData eventData)
    {
        SetDraggedPosition(eventData);
        //transform.position = m_dragStartPosition;
    }

    /// <summary>
    /// 设置图片位置方法
    /// </summary>
    /// <param name="eventData"></param>
    private void SetDraggedPosition(PointerEventData eventData)
    {
        //存储当前鼠标所在位置
        Vector3 globalMousePos;
        //UI屏幕坐标转换为世界坐标
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(m_rt, eventData.position, eventData.pressEventCamera, out globalMousePos))
        {
            //设置位置及偏移量
            m_rt.position = globalMousePos + m_offset;
        }
    }
}

