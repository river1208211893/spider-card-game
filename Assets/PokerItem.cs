﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.U2D;
using UnityEngine.UI;
using DG.Tweening;


public class PokerItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Image image;

    public PokerData Data;
    public bool IsOpen;

    public PokerContent ParentGroup;
    
    public void MoveTo(Vector3 positon,float speed)
    {
        float time = Vector3.Distance(transform.position, positon) / speed;
        Tweener t = transform.DOMove(positon, time);
        //设置在最上面
        //t.OnComplete(() => { transform.SetAsLastSibling(); });
    }

    public void Open()
    {
        IsOpen = true;
        SpriteAtlas atlas = Resources.Load<SpriteAtlas>("UI");
        image.sprite = atlas.GetSprite(Data.Type + "-" + Data.number);
    }

    public void Close()
    {
        IsOpen = false;
        SpriteAtlas atlas = Resources.Load<SpriteAtlas>("UI");
        image.sprite = atlas.GetSprite("back");
    }


    //存储图片中心点与鼠标点击点的偏移量 做什么
    private Vector3 m_offset;
    private Vector3 m_dragStartPosition;
    //存储当前拖拽图片的RectTransform组件
    private RectTransform m_rt;
    void Start()
    {
        //初始化
        m_rt = gameObject.GetComponent<RectTransform>();
    }
    //检查能不能一起拖
    bool CanDrag()
    {
        if (!PokerManager.It.canMove) return false;
        if (!IsOpen) return false;
        if (ParentGroup == null || ParentGroup.pointType != PokerContent.PointType.Top) return false;

        draggintItems = ParentGroup.GetItemWithNext(this);
        for(int i=0;i< draggintItems.Count-1;i++)
        {//如果当前移动的牌和另一个牌type不同不能移动，如果两个数字前后相差不是1不能移动
            if (draggintItems[i].Data.Type != draggintItems[i + 1].Data.Type)
                return false;
            if (draggintItems[i].Data.number - draggintItems[i + 1].Data.number != 1)
                return false;
        }

        return true;
    }

    //开始拖拽触发
    bool dragging = false;
    List<PokerItem> draggintItems;
    public void OnBeginDrag(PointerEventData eventData)
    {
        if(!CanDrag())
        {
            dragging = false;
            return;
        }
        dragging = true;
        image.raycastTarget = false;
        m_dragStartPosition = transform.position;
        // 存储点击时的鼠标坐标
        Vector3 tWorldPos;
        //UI屏幕坐标转换为世界坐标
        RectTransformUtility.ScreenPointToWorldPointInRectangle(m_rt, eventData.position, eventData.pressEventCamera, out tWorldPos);
        //计算偏移量   
        m_offset = transform.position - tWorldPos;
        for (int i = 0; i < draggintItems.Count; i++)
        {
            draggintItems[i].transform.SetAsLastSibling();
        }
    }

    //拖拽过程中触发
    public void OnDrag(PointerEventData eventData)
    {
        if(dragging)
            SetDraggedPosition(eventData);
    }

    //结束拖拽触发
    public void OnEndDrag(PointerEventData eventData)
    {
        if(dragging)
        {
            Vector3 pos;
            RectTransformUtility.ScreenPointToWorldPointInRectangle((RectTransform)transform, eventData.position, eventData.enterEventCamera, out pos);
            bool seccess = false;
            if (eventData.pointerCurrentRaycast.gameObject != null)
            {
                PokerItem drag = eventData.pointerCurrentRaycast.gameObject.GetComponent<PokerItem>();
                if (drag != null)
                {
                    //是某列的最后一个
                    if (drag.ParentGroup && drag.ParentGroup.IsLastOne(drag))
                    {
                        if (/*Data.Type == drag.Data.Type &&*/ drag.Data.number - Data.number == 1)
                        {
                            seccess = true;
                            //移动
                            ParentGroup.Delete(draggintItems);
                            drag.ParentGroup.Add(draggintItems);
                            drag.ParentGroup.checkDonePoker();
                        }
                    }
                }
                else
                {
                    //如果content一个item都没有 一个移上去
                    PokerContent content = eventData.pointerCurrentRaycast.gameObject.GetComponent<PokerContent>();
                    if (content != null && content.pointType== PokerContent.PointType.Top
                        && content.Length() == 0)
                    {
                        seccess = true;
                        //移动
                        ParentGroup.Delete(draggintItems);
                        content.Add(draggintItems);
                        content.checkDonePoker();
                    }
                }
                
            }
            if(!seccess)
            {
                
                for (int i = 1; i < draggintItems.Count; i++)
                {
                    Vector3 offset2 = draggintItems[i].m_rt.position - m_rt.position;
                    draggintItems[i].m_rt.position = m_dragStartPosition + offset2;
                }
                m_rt.position = m_dragStartPosition;
            }
        }
        image.raycastTarget = true;
    }

    /// <summary>
    /// 设置图片位置方法
    /// </summary>
    /// <param name="eventData"></param>
    private void SetDraggedPosition(PointerEventData eventData)
    {
        //存储当前鼠标所在位置
        Vector3 globalMousePos;
        //UI屏幕坐标转换为世界坐标
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(m_rt, eventData.position, eventData.pressEventCamera, out globalMousePos))
        {
            //设置位置及偏移量
            
            for (int i = 1; i < draggintItems.Count; i++)
            {
                Vector3 offset2 = draggintItems[i].m_rt.position - m_rt.position;
                draggintItems[i].m_rt.position = globalMousePos + m_offset + offset2;
            }
            m_rt.position = globalMousePos + m_offset;

        }
    }

}
