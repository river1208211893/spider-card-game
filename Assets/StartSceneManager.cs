﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartSceneManager : MonoBehaviour {

    public GameObject StartPanel;
    public Button ToDescPanelBtn;
    public Button ToDiffcuPanelBtn;
    public Button ToSettingBtn;
    public GameObject DescPanel;
    public Button DescBackBtn;
    public GameObject SettingPanel;
    public Button SettingBackBtn;
    public Button SettingDataBtn;
    public GameObject DiffcuPanel;
    public Button DiffcuBackBtn;
    public Button DiffcuEasyBtn;
    public Button DiffcuHaldBtn;
    public GameObject DataPanel;
    public Button DataBackBtn;
    public Text DataGameCountT;
    public Text DataGameTimeT;

    // Use this for initialization
    void Start () {

        ToDescPanelBtn.onClick.AddListener(()=> {
            OpenPanel(PanelType.desc);
        });
        ToDiffcuPanelBtn.onClick.AddListener(() => {
            OpenPanel(PanelType.deffcu);
        });
        ToSettingBtn.onClick.AddListener(() => {
            OpenPanel(PanelType.setting);
        });
        DescBackBtn.onClick.AddListener(() => {
            OpenPanel(PanelType.main);
        });
        SettingBackBtn.onClick.AddListener(() => {
            OpenPanel(PanelType.main);
        });
        SettingDataBtn.onClick.AddListener(() => {
            OpenPanel(PanelType.data);
            DataGameCountT.text = "Game Played：" + PlayerPrefs.GetInt("GameCount", 0);
            string mintimeStr = "none";
            if (PlayerPrefs.HasKey("minTime"))
            {
                int mintime = PlayerPrefs.GetInt("minTime");
                mintimeStr = PokerManager.IntToTime(mintime);
            }
            DataGameTimeT.text = "Shortest Time：" + mintimeStr;
        });

        DataBackBtn.onClick.AddListener(() => {
            OpenPanel(PanelType.setting);
        });
        DiffcuBackBtn.onClick.AddListener(() => {
            OpenPanel(PanelType.main);
        });

        DiffcuEasyBtn.onClick.AddListener(() => {
            PokerManager.pokerDifficult = 1;
            SceneManager.LoadScene(1);
        });

        DiffcuHaldBtn.onClick.AddListener(() => {
            PokerManager.pokerDifficult = 2;
            SceneManager.LoadScene(1);
        });
    }

	
    public enum PanelType
    {
        main,desc,setting,data,deffcu
    }

    public PanelType CurrentPanel = PanelType.main;

    public void OpenPanel(PanelType type)
    {
        CurrentPanel = type;

        StartPanel.SetActive(false);
        SettingPanel.SetActive(false);
        DescPanel.SetActive(false);
        DataPanel.SetActive(false);
        DiffcuPanel.SetActive(false);

        switch(type)
        {
            case PanelType.main:
                StartPanel.SetActive(true);
                break;
            case PanelType.setting:
                SettingPanel.SetActive(true);
                break;
            case PanelType.desc:
                DescPanel.SetActive(true);
                break;
            case PanelType.data:
                DataPanel.SetActive(true);
                break;
            case PanelType.deffcu:
                DiffcuPanel.SetActive(true);
                break;
        }
    }

	// Update is called once per frame
	void Update () {
		
	}
}
