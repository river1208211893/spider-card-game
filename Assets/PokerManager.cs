﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public struct PokerData
{
    public string Type;//a ,b,c,d
    public int number;//1-13
}

public class PokerManager : MonoBehaviour
{
    public GameObject InitSendMask;

    public static PokerManager It;
    public bool canMove=true;//当前如果在播动画 则不可做任何操作

    public Transform InitContent;
    public PokerItem PokerItemPrefab;
    //三种牌的位置
    public PokerContent[] GameTopPoints;//可操作的牌组
    public PokerContent[] rightBottomPokerPoints;//还没发的牌组
    public PokerContent[] leftBottomPokerPoints;//不可操作牌组

    public Button AddPokerBtn;

    public UndoManager UndoManager;
    public Button UndoBtn;
    public Button NewBtn;
    public Button RetureBtn;
    public Text TimeT;

    public GameObject GameOverPanel;
    public Text GameOverT;
    public Button GameOverBtn;


    float Timer;

    public static int pokerDifficult = 1;
    string[] pokerDataTypes = new string[] { "c", "c", "c", "c" };
    int[] PokerDataNums = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

    void Awake() { It = this; }

    void Start()
    {
        Init();

        AddPokerBtn.onClick.AddListener(AddPokerBtnClick);
        UndoBtn.onClick.AddListener(OnUndoBtnClick);
        NewBtn.onClick.AddListener(OnNewBtnClick);
        RetureBtn.onClick.AddListener(OnRetureBtnClick);
        GameOverBtn.onClick.AddListener(OnRetureBtnClick);
    }

    void Update()
    {
        if (!GameOverPanel.gameObject.activeSelf)
        {
            Timer += Time.deltaTime;
            ShowTime();
        }
    }

    void ShowTime()
    {
        int time = (int)Timer;
        
        string timeStr = IntToTime(time);
        TimeT.text = timeStr;
    }


    public void Init()
    {
        ChangeGameDifficult(pokerDifficult);//改变游戏难度
        initRandomPoker();//生成104张牌
        Shuffle();//乱序
        initPokerCard();//（此时未发牌）生成牌，添加到右下组
        StartCoroutine(initFirstSendPoker(rightBottomPokerPoints[rightBottomPokerPoints.Length-1], 54));
    }

    void ChangeGameDifficult(int diff)
    {
        switch (diff)
        {
            case 1:
                pokerDataTypes = new string[] { "c", "c", "c", "c" };
                break;
            case 2:
                pokerDataTypes = new string[] { "c", "d", "c", "d" };
                break;
        }
    }

    List<PokerData> AllPoker;
    List<PokerItem> AllPokerItems;
    //还剩多少牌没有发
    int L_TotalPokerCount;
    //生成牌数据
    public void initRandomPoker()
    {
        AllPoker = new List<PokerData>();
        for (var i = 0; i < pokerDataTypes.Length; i++)
        {
            for (var t = 1; t <= PokerDataNums.Length; t++)
            {
                //两幅牌 增加两次
                AllPoker.Add(new PokerData() { Type = pokerDataTypes[i], number = PokerDataNums[t - 1] });
                AllPoker.Add(new PokerData() { Type = pokerDataTypes[i], number = PokerDataNums[t - 1] });
            }
        }
    }
    //打乱牌数据
    void Shuffle()
    {
        for (var i = 0; i < AllPoker.Count - 1; i++)
        {
            var idx = Random.Range(i + 1, AllPoker.Count);
            var temp = AllPoker[idx];
            AllPoker[idx] = AllPoker[i];
            AllPoker[i] = temp;
        }
    }

    // 生成扑克牌卡片
    void initPokerCard()
    {
        AllPokerItems = new List<PokerItem>();

        for (var i = 0; i < AllPoker.Count; i++)
        {
            int pointIndex = i / 10; //🌟共生成104张牌
            pointIndex = Mathf.Clamp(pointIndex, 0, 4);
            PokerItem item = Instantiate(PokerItemPrefab, InitContent); //牌的实例化 
            item.Data = AllPoker[i];
            item.transform.position = rightBottomPokerPoints[pointIndex].transform.position;
            rightBottomPokerPoints[pointIndex].Add(item);
            AllPokerItems.Add(item);
        }

        L_TotalPokerCount= AllPokerItems.Count;
        Debug.LogError("初始牌  张数  " + L_TotalPokerCount);
    }

    void AddPokerBtnClick()
    {
        //上面有空位置  不允许发牌
        for(int i=0;i<GameTopPoints.Length;i++)
        {
            if (GameTopPoints[i].Length() == 0)
                return;
        }
        for(int i= rightBottomPokerPoints.Length-1; i>=0;i--)
        {
            if(rightBottomPokerPoints[i].Length()>0)
            {
                StartCoroutine(initFirstSendPoker(rightBottomPokerPoints[i], 10, false));//
                break;
            }
        }
    }
    
    // 首次发牌动画
    IEnumerator initFirstSendPoker(PokerContent sendTarget,int num,bool isFirst=true)
    {
        canMove = false;
        yield return null;
        if (isFirst)
        {
            InitSendMask.gameObject.SetActive(true);
        }
        //待转移的poker
        List<PokerItem> undoItems=new List<PokerItem>();
        int maxNum = Mathf.Min(sendTarget.Length(), num);

        for (int i= 0; i< maxNum; i++)
        {
            int topIndex = i % 10;
            //获取最后一个  重列表中删除
            PokerItem item = sendTarget.GetLastOne();
            sendTarget.Delete(new List<PokerItem>() { item },false);
            //添加到undo列表中
            undoItems.Add(item);
            GameTopPoints[topIndex].Add(item);
            L_TotalPokerCount--;
            if(!isFirst)
             yield return new WaitForSeconds(0.1f);
        }
        
        //不是第一发牌
        if (!isFirst)
        {
            UndoManager.Add(sendTarget, undoItems, undoItems);
        }
        //Debug.LogError("还剩牌  张数 " + L_TotalPokerCount);
        yield return new WaitForSeconds(0.5f);
        for(int i=0;i< GameTopPoints.Length; i++)
        {
            GameTopPoints[i].OpenLastOne();
        }

        canMove = true;

        if(isFirst)
        {
            InitSendMask.gameObject.SetActive(false);
        }
    }

    #region btnEvent
    void OnUndoBtnClick()
    {
        UndoManager.Undo();
    }

    void OnNewBtnClick()
    {
        SceneManager.LoadScene(1);
    }

    void OnRetureBtnClick()
    {
        SceneManager.LoadScene(0);
    }

    public void GameOver()
    {
        Debug.LogError("游戏成功  结束");
        GameOverPanel.SetActive(true);
        GameOverT.text = "Time Spent: " + IntToTime((int)Timer);

        DatSave();
    }
    #endregion
 
    void DatSave()
    {
        //count
        int lastCount = PlayerPrefs.GetInt("GameCount", 0);
        PlayerPrefs.SetInt("GameCount", lastCount+1);

        //time
        if (PlayerPrefs.HasKey("minTime"))
        {
            int lastTime = PlayerPrefs.GetInt("minTime");
            if(Timer<lastTime)
            {
                PlayerPrefs.SetInt("minTime", (int)Timer);
            }
        }else
        {
            PlayerPrefs.SetInt("minTime", (int)Timer);
        }
    }

    

    public static string IntToTime(int time)
    {
        int m = time / 60;
        int s = time % 60;
        string timeStr = m + "m" + s + "s";
        return timeStr;
    }
}