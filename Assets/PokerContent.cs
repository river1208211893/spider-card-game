﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PokerContent : MonoBehaviour {
    public enum PointType
    {
        Top,LeftBottom,RightBottom
    }

    public PointType pointType= PointType.Top;

    //每张牌的高度偏移
    public int offset=50;
    [SerializeField]
    List<PokerItem> items = new List<PokerItem>();

	public void Add(PokerItem item)
    {
        int off_y = offset * items.Count;
        items.Add(item);
        item.MoveTo( transform.position + new Vector3(0, -off_y, 0),1500f);
        //设置在最上面
        item.transform.SetAsLastSibling();
        item.ParentGroup = this;
    }

    
    
    public void Add(List<PokerItem> _items)
    {
        for(int i=0;i< _items.Count;i++)
        {
            Add(_items[i]);
        }
    }
   

    public List<PokerItem> GetItemWithNext(PokerItem item)
    {
        int start = 0;
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] == item)
            {
                start = i; break;
            }
        }

        List<PokerItem> result = new List<PokerItem>();
        for(int i=start;i < items.Count; i++)
        {
            result.Add(items[i]);
        }
        return result;
    }

    public void Delete(List <PokerItem> _items,bool addToUndo=true)
    {
        for(int i=0;i< _items.Count;i++)
        {
            items.Remove(_items[i]);
        }

        bool lastIsOpen= OpenLastOne();
        //添加到undo
        if (addToUndo)
        {
            List<PokerItem> opens = new List<PokerItem>();
            if (lastIsOpen) opens.Add(items[items.Count - 1]);

            PokerManager.It.UndoManager.Add(this, _items,opens);
        }
    }

    public bool IsLastOne(PokerItem item)
    {
        if(items.Count>0)
        {
            return  items[items.Count - 1] == item;
        }
        return false;
    }

    public int Length()
    {
        return items.Count;
    }
    //返回是否真的执行了翻牌
    public bool OpenLastOne()
    {
        if (pointType != PointType.Top)
            return false;

        bool result = false;
        if (items.Count > 0)
        {
            result = !items[items.Count - 1].IsOpen;
            items[items.Count - 1].Open();
        }
        return result;
    }

    // 检测是否可以直接收取正确排序的卡牌
    public void checkDonePoker()
    {
        if (items.Count < 13)
            return;
        bool seccess = true;
        for(int i= items.Count-1; i>= items.Count - 13 + 1;i--)
        {
            if (!items[i -1].IsOpen)
            {
                seccess = false; break;
            }
            if (items[i].Data.Type != items[i - 1].Data.Type)
            {
                seccess = false; break;
            }
            if (items[i-1].Data.number - items[i].Data.number != 1)
            {
                seccess = false; break;
            }
        }

        if(seccess)
        {
            Debug.Log("完成一组牌");
            StartCoroutine(moveDonePoker());
        }
	}

    IEnumerator moveDonePoker()
    {
        PokerManager.It.canMove = false;
        PokerContent Target= PokerManager.It.leftBottomPokerPoints[0];
        bool GameOver = false;
        for(int i=0;i<PokerManager.It.leftBottomPokerPoints.Length;i++)
        {
            if(PokerManager.It.leftBottomPokerPoints[i].Length()==0)
            {
                Target = PokerManager.It.leftBottomPokerPoints[i];
                if (i == PokerManager.It.leftBottomPokerPoints.Length - 1)
                    GameOver = true;
                break;
            }
        }
        List<PokerItem> deleteItems=new List<PokerItem>();
        for (int i = 0; i <13 ; i++)
        {
            Target.Add(items[items.Count - i - 1]);
            deleteItems.Insert(0,items[items.Count - i - 1]);
            yield return new WaitForSeconds(0.1f);
        }
        Delete(deleteItems);
        yield return new WaitForSeconds(0.5f);
        PokerManager.It.canMove = true;
        if(GameOver)
        {
            PokerManager.It.GameOver();
        }
    }

    public PokerItem GetLastOne()
    {
        if (items.Count > 0)
        {
            return items[items.Count - 1];
        }
        return null;
    }
}
